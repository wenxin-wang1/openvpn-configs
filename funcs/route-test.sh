#!/bin/bash

. $(cd $(dirname ${BASH_SOURCE[0]}) && pwd)/common.sh

route_test_ns_init() {
    if [ $# -ne 1 ]; then
        echo "usage: ${FUNCNAME[0]} nsname"
        return 1
    fi
    local name=$1
    sudo ip netns add  $name
    sudo ip netns exec $name ip tuntap add mode tun tun0
    sudo ip netns exec $name ip l set tun0 up
    sudo ip netns exec $name ip a add 127.0.0.1/8 dev tun0
    sudo ip netns exec $name ip -6 a add fec0::1/96 dev tun0
}

route_test_ns_destroy() {
    if [ $# -ne 1 ]; then
        echo "usage: ${FUNCNAME[0]} nsname"
        return 1
    fi
    local name=$1
    sudo ip netns del $name
}

route_test_add4() {
    if [ $# -ne 2 ]; then
        echo "usage: ${FUNCNAME[0]} nsname route4"
        return 1
    fi
    local name=$1
    local route=$2
    sudo ip netns exec $name ip r add $route via 127.0.0.2
}

route_test_add6() {
    if [ $# -ne 2 ]; then
        echo "usage: ${FUNCNAME[0]} nsname route6"
        return 1
    fi
    local name=$1
    local route=$2
    sudo ip netns exec $name ip r add $route via fec0::2
}

route_test_ip_assert_route() {
    if [ $# -ne 2 ]; then
        echo "usage: ${FUNCNAME[0]} nsname ip"
        return 1
    fi
    local name=$1
    local ip=$2
    route=$(sudo ip netns exec $name ip r get $ip fibmatch 2>/dev/null)
    ret=$?
    if [ $ret -ne 0 ]; then
        return $ret
    fi
    cut -d' ' -f1 <<<"$route"
}

route_test_ip_has_route() {
    route_test_ip_assert_route $@ >/dev/null
}

route_test_ip_get_route() {
    route_test_ip_assert_route $@ || true
}

route_test_prefix_assert_route() {
    if [ $# -ne 2 ]; then
        echo "usage: ${FUNCNAME[0]} nsname prefix"
        return 1
    fi
    local name=$1
    local prefix=$2
    local network preflen
    IFS='/' read -r network preflen <<<$prefix
    assert_var_non_empty network
    route=$(route_test_ip_assert_route $name $prefix)
    ret=$?
    [ $ret -eq 0 ] || return $ret
    [ z"$preflen" != z ] || { echo $route; return; }
    local mnet mlen
    IFS='/' read_into_vars_assert mnet mlen <<<$route
    [ $mlen -le $preflen ] && echo $route
}

route_test_prefix_has_route() {
    route_test_prefix_assert_route $@ >/dev/null
}

route_test_prefix_get_route() {
    route_test_prefix_assert_route $@ || true
}
