#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 5 ]; then
    echo "usage: $0 server port pki_dir prefix tun_addr"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
__BASE__=$(cd $__DIR__/.. && pwd)
. $__BASE__/funcs/common.sh

server=$1
port=$2
pki_dir=$3
prefix=$4
tun_addr=$5

IFS='/' read_into_vars_assert network preflen <<<$prefix

cat <<EOF
mode server
tls-server
dev tun
port $port
proto udp6

cipher none
auth SHA512
auth-nocache

keepalive 60 120

# Dummy IPv4 address, to make OpenVPN works and
# configure IPv6 address on its tun device
server 169.254.0.0 255.255.0.0
ifconfig-ipv6 $tun_addr/$preflen $network
push tun-ipv6
client-config-dir /etc/openvpn/ccd
ccd-exclusive

verb 3

key-direction 0

<ca>
EOF
cat $pki_dir/ca/pki/ca.crt
cat <<EOF
</ca>

<cert>
EOF

cat $pki_dir/ca/pki/issued/$server.crt
cat <<EOF
</cert>

<key>
EOF
cat $pki_dir/server/pki/private/$server.key
cat <<EOF
</key>

<dh>
EOF
cat $pki_dir/server/secrets/$server-dh.pem
cat <<EOF
</dh>

<tls-auth>
EOF
cat $pki_dir/server/secrets/$server-ta.key
cat <<EOF
</tls-auth>
EOF
