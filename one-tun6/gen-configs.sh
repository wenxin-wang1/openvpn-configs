#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
__BASE__=$(cd $__DIR__/.. && pwd)
. $__BASE__/funcs/common.sh

dir=$1

for l in $(_list_d $dir/server); do
    while read -u 3 -r line; do
        read_into_vars_assert server port pki_dir sub_prefix tun_addr <<<"$line"
        echo $server $sub_prefix
        mkdir -p $dir/$server/{server/ccd,clients}
        $__DIR__/gen-server-config.sh $server $port $pki_dir $sub_prefix $tun_addr >$dir/$server/server/server.conf
        touch $dir/$server/client.list
        for l in $(_list_d $dir/$server/client); do
            while read -u 3 -r line; do
                read_into_vars_assert client addr <<<"$line"
                echo $client $addr
                $__DIR__/gen-client-config.sh $client $server $port $pki_dir >$dir/$server/clients/$client.ovpn
                cat <<EOF >$dir/$server/server/ccd/$client
ifconfig-ipv6-push $addr
push "redirect-gateway ipv6 !ipv4"
EOF
            done 3< <(grep -v '^\s*#' $l)
        done
    done 3< <(grep -v '^\s*#' $l)
done
