#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 4 ]; then
    echo "usage: $0 client server port pki_dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
__BASE__=$(cd $__DIR__/.. && pwd)
. $__BASE__/funcs/common.sh

client=$1
server=$2
port=$3
pki_dir=$4

cat <<EOF
client

dev tun

cipher none
auth SHA512
auth-nocache

remote $server $port udp

resolv-retry infinite
nobind
remote-cert-tls server

verb 3

key-direction 1

<ca>
EOF
cat $pki_dir/ca/pki/ca.crt
cat <<EOF
</ca>

<cert>
EOF

cat $pki_dir/ca/pki/issued/$client.crt
cat <<EOF
</cert>

<key>
EOF
cat $pki_dir/client/pki/private/$client.key
cat <<EOF
</key>

<tls-auth>
EOF
cat $pki_dir/server/secrets/$server-ta.key
cat <<EOF
</tls-auth>
EOF
