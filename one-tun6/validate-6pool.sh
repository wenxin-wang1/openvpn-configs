#!/bin/bash

set -e

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
__BASE__=$(cd $__DIR__/.. && pwd)
. $__BASE__/funcs/route-test.sh

dir=$1

onerror() {
    route_test_ns_destroy $NS_TOTAL || true
    route_test_ns_destroy $NS_SUB || true
    route_test_ns_destroy $NS_SVR || true
    >&2 echo Error on line $LINENO
}

trap onerror ERR

NS_TOTAL=NS_TOTAL
NS_SUB=NS_SUB
NS_SVR=NS_SVR

route_test_ns_init $NS_TOTAL
route_test_ns_init $NS_SUB

for l in $(_list_d $dir/prefix6); do
    while read -u 3 -r line; do
        read_into_vars_assert prefix <<<"$line"
        route_test_prefix_get_route $NS_TOTAL $prefix
        ! route_test_prefix_has_route $NS_TOTAL $prefix  || complain 1 "pool $prefix overlaps!"
        route_test_add6 $NS_TOTAL $prefix
    done 3< <(grep -v '^\s*#' $l)
done

for l in $(_list_d $dir/server); do
    while read -u 3 -r line; do
        read_into_vars_assert server port pki_dir sub_prefix <<<"$line"
        route_test_prefix_has_route $NS_TOTAL $sub_prefix || complain 1 "$server sub_prefix $sub_prefix not in pool!"
        ! route_test_prefix_has_route $NS_SUB $sub_prefix || complain 1 "$server sub_prefix $sub_prefix overlaps!"
        route_test_add6 $NS_SUB $sub_prefix

        route_test_ns_init $NS_SVR
        route_test_add6 $NS_SVR $sub_prefix
        for l in $(_list_d $dir/$server/client); do
            while read -u 3 -r line; do
                read_into_vars_assert client addr <<<"$line"
                route_test_ip_has_route $NS_SVR $addr || complain 1 "$server $client $addr not in pool!"
            done 3< <(grep -v '^\s*#' $l)
        done
        route_test_ns_destroy $NS_SVR
    done 3< <(grep -v '^\s*#' $l)
done

route_test_ns_destroy $NS_TOTAL
route_test_ns_destroy $NS_SUB
